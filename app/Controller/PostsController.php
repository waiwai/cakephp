<?php
App::uses('AppHelper', 'View/Helper');
App::uses('CookieComponent','Component');
App::uses('AppController', 'Controller');

class PostsController extends AppController {
var $uses=array('Post','Comment');
 public $helpers = array('Html', 'Form',"Flash");
 public $components = array('Paginator','Session');

 public function logout()
   {

      $this->Session->delete('User');

      $this->Session->destroy();

     $this->redirect($this->Auth->logout());

   }
 
      public function index()
    {
        
      //debug($this->Post->find('all'));
     
        $this->set('posts',$this->Post->find('all'));
        $this->paginate=array('limit'=>10);
        $posts = $this-> paginate('Post');
        $this->set('posts', $posts);
    
  
  //search form by keywords
        $title = array();
       if(isset($this->passedArgs['Search.keywords']))

        {
              $keywords = $this->passedArgs['Search.keywords'];
              $this->paginate =array(
                                     'conditions'=>array(
                                     'OR'=>array(
                                             'Post.id LIKE'=>$keywords,
                                             'Post.title LIKE' =>'%' .$keywords.'%',
                                             'Post.body LIKE'=>'%' .$keywords.'%'
                                         )
                                       )
                 
                                     );
            
             $posts = $this-> paginate('Post');
             $this->set('posts', $posts);
             $this->request->data['Search']['keywords'] = $keywords;
            $title[] = __('Keywords',true).': '.$keywords;
        }

            if(isset($this->passedArgs['Search.id'])) 
        {
            $id = $this->passedArgs['Search.id'];
            $this->paginate =array(
                                   'conditions'=>array(
                                   'OR'=>array(
                                    'Post.id LIKE'=>$id 

                                     )
                                   )     
                 
                                 );
            
            $posts = $this-> paginate('Post');
            $this->set('posts', $posts);
            $this->request->data['Search']['id'] = $id;
            $title[] = __('id',true).': '.$id;
        }

//body
        if(isset($this->passedArgs['Search.body'])) 
        {
            $body = $this->passedArgs['Search.body'];
           $this->paginate =array(
                                  'conditions'=>array(
                                  'OR'=>array(
                                  'Post.body LIKE'=>'%'.$body.'%'
                            
                                )
                             )
                          );
            
          $posts = $this-> paginate('Post');
          $this->set('posts', $posts);
          $this->request->data['Search']['body'] = $body;
          $title[] = __('body',true).': '.$body;
        }
        //title

        if(isset($this->passedArgs['Search.title']))
       {
           $title = $this->passedArgs['Search.title'];
           $this->paginate =array(
                                  'conditions'=>array(
                                  'OR'=>array(
                                 'Post.title LIKE'=>'%'.$title.'%'
                                )
                              )
                            );
            
          $posts = $this-> paginate('Post');
          $this->set('posts', $posts);
          $this->request->data['Search']['body'] = $body;
          $title[] = __('body',true).': '.$body;
        }

    }

    public function view($id=null)
    {
        App::import('UserController','Controller');
      
        $Username=$_SESSION['Auth']['User']['username'];
    
        $this->Post->id=$id;
 
        $this->set('posts',$this->Post->read());
        
        

         
     }
      public function viewcomment($id=null)
    {
        App::import('UserController','Controller');
      
        $Username=$_SESSION['Auth']['User']['username'];
    
        $this->Post->id=$id;
 
        $this->set('posts',$this->Post->read());
         if(!empty($this->request->data)){

        if(!empty($this->data['Comment']))
        { 
           if(!empty($this->data['Comment']['image']['name']))
           {
              $file=$this->request->data['Comment']['image'];
              $ext=substr(strtolower(strrchr($file['name'],'.')),1);
              $arr_ext=array('jpg','jpeg','gif','png');
                 if(in_array($ext,$arr_ext))
               {
                     if(move_uploaded_file($file['tmp_name'],WWW_ROOT.'img/'.$file['name']))                      
                      $this->request->data['Comment']['image'] = '/img/'.$file['name'];
                      pr($this->request->data);
            

              }

            
              }
              else
              {
                $this->request->data['Comment']['image']='null';
              }
               
                 
                  $this->request->data['Comment']['class']='Post';
                  $this->request->data['Comment']['foreign_id']=$id;
                  $this->request->data['Comment']['name']=$Username;
                 
                  if($this->Post->Comment->save($this->data))
                  {
                    
                     $this->redirect(array('controller'=>'posts','action'=>'view',$id));

                  }
              }
        
              }
}
         
    
    

    public function add()
    {

        if($this->request->is('post'))
        {
          $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                if(!empty($this->data)){
          if(!empty($this->data['Post']['image']['name']))
           {
              $file=$this->request->data['Post']['image'];
              $ext=substr(strtolower(strrchr($file['name'],'.')),1);
              $arr_ext=array('jpg','jpeg','gif','png');
                 if(in_array($ext,$arr_ext))
               {
               move_uploaded_file($file['tmp_name'],WWW_ROOT.'img/'.$file['name']);
                      
                      $this->request->data['Post']['image'] = '/img/'.$file['name'];
                      pr($this->request->data);
            
               
              }
            
              }
            }
           
            if($this->Post->save($this->request->data))
            {
                $this->Session->setFlash('Your post has been saved');
               $this->redirect(array('action' => 'index'));
            }
            else
            {
                $this->Session->setFlash('Unable to add your post');

            }

        }
}
    
   
  
    public function edit($id=null)
    
    {
        $this->Post->id=$id;
        if(!empty($this->data['Post']['image']['name']))
           {
              $file=$this->request->data['Post']['image'];
              $ext=substr(strtolower(strrchr($file['name'],'.')),1);
              $arr_ext=array('jpg','jpeg','gif','png');
                 if(in_array($ext,$arr_ext))
               {
                     if(move_uploaded_file($file['tmp_name'],WWW_ROOT.'img/'.$file['name']))                      
                      $this->request->data['Post']['image'] = '/img/'.$file['name'];
                      pr($this->request->data);
               }
            
            }

              
        if($this->request->is('get'))
        {
           $this->request->data=$this->Post->read();

        }

        else

        {

            if($this->Post->save($this->request->data))
            {
               $this->Session->setFlash('Your post has been updated.');
               $this->redirect(array('action'=>'index'));

            }

           else
           {
              
                $this->Session->setFlash('Unable to update your post.');

           }

      }
    }
     public function editcomment($id=null,$foreign_id=null)
    
    {
        $this->Comment->id=$id;
        $this->Post->id=$foreign_id;
          if(!empty($this->request->data))
          {

            if(!empty($this->request->data['Comment']['image']['name']))
              {
                $file=$this->request->data['Comment']['image'];
                $ext=substr(strtolower(strrchr($file['name'],'.')),1);
                $arr_ext=array('jpg','jpeg','gif','png');
                 if(in_array($ext,$arr_ext))
                {
                     if(move_uploaded_file($file['tmp_name'],WWW_ROOT.'img/'.$file['name'])) {
                              
                      $this->request->data['Comment']['image'] = '/img/'.$file['name'];
                      pr($this->request->data);
                      }
                  

              }
            
              } 

            }

        if($this->request->is('get'))
        {
           $this->request->data=$this->Comment->read();

        }
        else

        {

            if($this->Comment->save($this->request->data))
            {
                $this->redirect(array('controller'=>'posts','action'=>'view/',$foreign_id));


            }

           else
           {
              
                $this->Session->setFlash('Unable to update your post.');

           }

      }
    }

     public function deleteComment($id,$foreign_id)
    {
     
       if($this->request->is('get'))
        {

            throw new MehtodNotAllowException();

        }
        if($this->Comment->delete($id))
        {
           
            $this->redirect(array('controller'=>'posts','action'=>'view/',$foreign_id));

        }
 

        

    }

     public function delete($id)
    {

       if($this->request->is('get'))
        {

            throw new MehtodNotAllowException();

        }

        if($this->Post->delete($id))
        {

            $this->Session->setFlash('The post delete postcontroler with id:.'. $id . 'has been deleted');
            $this->redirect(array('action'=>'index'));
        }

    }

     public function isAuthorized($user) 
   {
    // All registered users can add posts
       if ($this->action === 'add')
       {

        return true;

       }

    // The owner of a post can edit and delete it
      if (in_array($this->action, array('edit', 'delete')))
       {
         $postId = (int) $this->request->params['pass'][0];

         if ($this->Post->isOwnedBy($postId, $user['id']))
         {
            return true;
         }

       }

    return parent::isAuthorized($user);
  }



//search form
    var $name = 'Posts';

    function search() 
    {
        // the page we will redirect to
        $url['action'] = 'index';

         foreach ($this->data as $k=>$v){ 
             foreach ($v as $kk=>$vv){ 
                if ($vv) {
                    $url[$k.'.'.$kk]=$vv; 
                }
            } 
        }

        // redirect the user to the url
        $this->redirect($url, null, true);
    }
 }

