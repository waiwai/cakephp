<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('AppHelper', 'View/Helper');
class UsersController extends AppController {

 public $helpers = array('Html', 'Form',"Flash");
 public $components=array('Session');
 public $paginate = array(
                        'limit' => 4,
                        'conditions' => array('status' => '1'),
                        'order' => array('User.username' => 'asc' ) 
    );
     
    public function beforeFilter() 
    {
      
        $this->Auth->allow('login','add','edit','delete','index','logout'); 
         
        $this->Auth->deny('view');
    
    }
     //add display new
    public function display() {

    }
 
    public function login() {
         if($this->Session->check('Auth.User')){
            $this->redirect(array('controller'=>'posts','action'=>'index'));
         }
       
                          
           if ($this->request->is('post')) 
        {
            if ($this->Auth->login()) 
            {            
                
                     $this->Session->setFlash(__('Welcome, '. $this->Auth->user('username')));
                     $this->redirect(array('controller'=>'posts','action' => 'index'));
            }
              else
                {
                     $this->Session->setFlash(__('Invalid username or password'));
                }
         } 
    
 }
   public function logout()
   {

      $this->Session->delete('User');

      $this->Session->destroy();

      $this->redirect($this->Auth->logout());

   }
 
    public function index() {
        $this->paginate = array(
            'limit' => 6,
            'order' => array('User.username' => 'asc' )
        );
        $users = $this->paginate('User');
        $this->set(compact('users'));
       }
    
 
 
    public function add() {
       if ($this->request->is('post')){

                 $this->User->create();
                 if ($this->User->save($this->request->data))
             {
                $this->Session->setFlash(__('The user has been created'));
                $this->redirect(array('controller'=>'users','action' => 'index'));
            }  else 

            {
                $this->Session->setFlash(__('The user could not be created. Please, try again.'));
            }   
                           
        }
    
 }
    public function edit($id = null) {
 
            if (!$id) {
                $this->Session->setFlash('Please provide a user id');
                $this->redirect(array('action'=>'index'));
            }
 
            $user = $this->User->findById($id);
            if (!$user) {
                $this->Session->setFlash('Invalid User ID Provided');
                $this->redirect(array('action'=>'index'));
            }
 
            if ($this->request->is('post') || $this->request->is('put')) 
            {
                $this->User->id = $id;
                if ($this->User->save($this->request->data))
                {
                    $this->Session->setFlash(__('The user has been updated'));
                    $this->redirect(array('action' => 'edit', $id));
                }

                else
                {
                    $this->Session->setFlash(__('Unable to update your user.'));
                }
            }
 
            if (!$this->request->data) 
            {
                $this->request->data = $user;
            }
    }
 
   public function delete($id = null) {
         
      /*  if (!$id) {
            $this->Session->setFlash('Please provide a user id');
            $this->redirect(array('action'=>'index'));
        }
         
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
            $this->redirect(array('action'=>'index'));
        }
        if ($this->User->saveField('status', 0)) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }
      //if($this->request->is('get'))
      //  {

       //     throw new MehtodNotAllowException();

       // }
*/
        if($this->User->delete($id))
        {

            $this->Session->setFlash('The post with id:.'. $id . 'has been deleted');
            $this->redirect(array('action'=>'index'));
        }
    }
    
    //activate

    /*public function activate($id = null) {
         
        if (!$id) {
            $this->Session->setFlash('Please provide a user id');
            $this->redirect(array('action'=>'index'));
        }
         
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
            $this->redirect(array('action'=>'index'));
        }
        if ($this->User->saveField('status', 1)) {
            $this->Session->setFlash(__('User re-activated'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not re-activated'));
        $this->redirect(array('action' => 'index'));
    }*/
 
}