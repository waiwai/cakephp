<h1>
Add Post
</h1>
<?php
        echo $this->Form->input('title');
        echo $this->Form->input('subcategory_id');
   
$this->Js->get('#PostCategoryId')->event('change', 
    $this->Js->request(array(
        'controller'=>'subcategories',
        'action'=>'getByCategory'
        ), array(
        'update'=>'#PostSubcategoryId',
        'async' => true,
        'method' => 'post',
        'dataExpression'=>true,
        'data'=> $this->Js->serializeForm(array(
            'isForm' => true,
            'inline' => true
            ))
        ))
    );