<?php
echo $this->Html->link( "Logout",   array('action'=>'logout') ); 
?>
<h2>Blog posts</h2>
<?php 
echo $this->Html->link(__('Add New Post'),array('action'=>'add'));
//search form

echo $this->Form->create('Post',array('url'=>'search'));?>
    <fieldset>
        <legend>
        <?php echo __('Post Search', true);?>
        </legend>
    <?php
        echo $this->Form->input('Search.keywords');
        echo "\n";
        echo"<table>";
        echo"<tr>";
        echo"<td>";
    	echo $this->Form->input('Search.id');
    	echo"</td>";
    	echo"<td>";
    	echo $this->Form->input('Search.body');
    	echo"</td>";
    	echo"<td>";
    	echo $this->Form->input('Search.title');
    	echo"</td>";
    	echo"<tr>";
        echo $this->Form->submit('Search');
    ?>
    </fieldset>
<?php echo $this->Form->end();?>
<?php
$paginator=$this->paginator;
if($posts){
	?>
<table>
<tr>
	<th>ID</th>
	<th>Title</th>
	<th>Body</th>
	<th>Created</th>
	<th>Modified</th>
	
</tr>
<?php foreach($posts as $post): ?>
<tr>
	<td><?php echo $post['Post']['id'];?></td>
	<td>
		<?php echo $this->Html->link($post['Post']['title'],
		array('controller'=> 'posts',
			'action'  =>  'view',
			$post['Post']['id']));?>
	</td>
	<td>
		<?php echo $this->Html->link($post['Post']['body'],
		array('controller'=> 'posts',
			'action'  =>  'view',
			$post['Post']['id']));?>
	</td>
	<td><?php echo $post['Post']['created'];?></td>
	<td><?php echo $post['Post']['modified'];?></td>
	
	
</tr>
<?php endforeach; ?>
</table>
<?php
 $paginator->options(array('url' => $this->passedArgs)); 
//paginator
 echo "<div class='paging'>";
 
        // the 'first' page button
        echo $paginator->first("First");
         
        if($paginator->hasPrev()){
            echo $paginator->prev("Prev");
        }
         
        // the 'number' page buttons
        echo $paginator->numbers(array('modulus' => 2));
         
        // for the 'next' button
        if($paginator->hasNext()){
            echo $paginator->next("Next");
        }
         
        // the 'last' page button
        echo $paginator->last("Last");
     
    echo "</div>";
    
}

else{
    echo "<span style='color: red;' />No results found.";
}

// tell the user there's no records found

?>

