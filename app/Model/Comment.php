<?php
class Comment extends AppModel {
    var $name = 'Comment';
    public $validate=array(
    	'image'=>array('rule'=>'notBlank'),
		'comment'=>array('rule'=>'notBlank')
		
		);
   public  $belongsTo = array('Post'=>array('className'=>'Post','foreignKey'=>'foreign_id'),
   								'User'=>array('className'=>'User','foreignKey'=>'name')
   							 );

}