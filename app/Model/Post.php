<?php
class Post extends AppModel{
	public $validate=array(
		'image'=>array('rule'=>'notBlank'),
		'title'=>array('rule'=>'notBlank'),
		'body' =>array('rule'=>'notBlank')
		);

var $name='Post';
var $belongsTo = array(
        'User' => array(
            'className'     => 'User',
            'foreignKey'    => 'user_id'
        )
    );
var $hasMany=array('Comment'=>array('className'=>'Comment',
									 'foreignKey'=>'foreign_id',
									'conditions'=>array('Comment.class'=>'Post'),
								

									));
public function isOwnedBy($post, $user)
 {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
 }
}